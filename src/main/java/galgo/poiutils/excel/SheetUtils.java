/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgo.poiutils.excel;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author valdemar.arantes
 */
public class SheetUtils {

    private static final Logger log = LoggerFactory.getLogger(SheetUtils.class);

    /**
     * Monta uma lista de Maps utilizando initialRowNr como sendo a primeira linha de uma série de
     * linhas e o intervalo de colunas definido entre initialColNr e finalColNr inclusive. A leitura
     * das linhas para quando ou a linha não está definida ou quando todas as células do intervalo
     * de uma linha são nulas. A primeira linha é considerada como sendo a Header Line.
     *
     * @param sheet
     * @param initialRowNr
     * @param initialColNr
     * @param finalColNr
     * <p>
     * @return
     */
    public static List<Map<String, String>> getCellsAsListOfMap(Sheet sheet, int initialRowNr,
            int initialColNr, int finalColNr) {
        List<Map<String, String>> list = Lists.newArrayList();
        int rowNr = initialRowNr + 1;

        // Recuperando o cabeçalho como uma lista de Strings
        Row hdrRow = sheet.getRow(initialRowNr);
        List<String> hdrList = Lists.newArrayList();
        for (int col = initialColNr; col <= finalColNr; col++) {
            hdrList.add(getCellAsString(hdrRow, col));
        }

        // Montando a lista de Maps
        do {
            Row row = sheet.getRow(rowNr);
            if (row == null) {
                break;
            }
            Map<String, String> map = Maps.newHashMap();
            boolean allCellsAreNull = true;
            for (int col = initialColNr; col <= finalColNr; col++) {
                try {
                    String cellValue = StringUtils.trimToNull(getCellAsString(row, col));
                    if (cellValue != null) {
                        allCellsAreNull = false;
                    }
                    map.put(hdrList.get(col - initialColNr), cellValue);
                } catch (Exception e) {
                    log.error("Erro ao ler célular[{},{}]", rowNr, col);
                    log.error(null, e);
                    continue;
                }
            }
            if (allCellsAreNull) {
                break;
            } else {
                list.add(map);
                rowNr++;
            }
        } while (true);
        return list;
    }

    private static String getCellAsString(Row hdrRow, int col) {
        return CellUtils.from(hdrRow.getCell(col)).getString();
    }

}
