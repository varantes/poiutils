/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgo.poiutils.excel;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Métodos utilitários para uma célula do Excel
 * <p/>
 *
 * @author valdemar.arantes
 */
public class CellUtils {

    private static final Logger log = LoggerFactory.getLogger(CellUtils.class);
    private Cell cell;

    public static CellUtils from(Cell cell) {
        return new CellUtils(cell);
    }
    
    /**
     * Obtém o valor BigDecimal da célula, seja ela do tipo numérico ou do tipo texto.
     *
     * @return O valor da célula como BigDecimal. Se estiver vazia, retorna null.
     */
    public BigDecimal getBigDecimal() {
        if ((cell == null) || (cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
            return null;
        }

        if (isString()) {
            String value = cell.getStringCellValue().replace(',', '.');
            try {
                return new BigDecimal(value);
            } catch (NumberFormatException e) {
                log.error("NumberFormatExceptione na célula [{}, {}] (valor={})",
                        cell.getRowIndex(), cell.getColumnIndex(), value);
                throw e;
            }
        }

        return new BigDecimal(NumberToTextConverter.toText(cell.getNumericCellValue()));
    }

    /**
     * Obtém o valor Integer da célula, seja ela do tipo numérico ou do tipo texto.
     *
     * @return O valor da célula como Integer. Se estiver vazia, retorna null.
     */
    public Integer getInt() {
        if ((cell == null) || (cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
            return null;
        }

        if (isString()) {
            String strValue = cell.getStringCellValue();
            if (StringUtils.isBlank(strValue)) {
                return null;
            }
            return Integer.valueOf(strValue);
        }

        double d = cell.getNumericCellValue();
        if (d % 1 == 0) {
            return Double.valueOf(cell.getNumericCellValue()).intValue();
        } else {
            throw new NumberFormatException(d + " não é um inteiro");
        }
    }
    
    /**
     * Obtém o valor String da célula.
     * </br></br>
     * Para campos numéricos, retorna a <Typo_Numérico>.toString() 
     * 
     * @return O valor da célular como uma String
     */
    public String getString() {
        if ((cell == null) || (cell.getCellType() == Cell.CELL_TYPE_BLANK)) {
            return null;
        }

        if (isString()) {
            return cell.getStringCellValue();
        } else {
            double doubleValue = cell.getNumericCellValue();
            if ((doubleValue % 1) == 0) {
                return Long.toString((long) doubleValue);
            } else {
                return Double.toString(doubleValue);
            }
        }
        
    }

    /**
     *
     * @return true se a c&eacute;lula for do tipo String ou se a f&oacute;rmula gera uma String
     */
    public boolean isString() {
        if ((cell == null)) {
            return false;
        }
        return (cell.getCellType() == Cell.CELL_TYPE_STRING) ||
                ((cell.getCellType() == Cell.CELL_TYPE_FORMULA) &&
                        (cell.getCachedFormulaResultType() == Cell.CELL_TYPE_STRING));
    }

    /**
     *
     * @return True se a célula estiver formatada como Data
     */
    public boolean isDate() {
        if (isString()) {
            return false;
        }
        return DateUtil.isCellDateFormatted(cell);
    }

    public Date getDate() {
        return cell.getDateCellValue();
    }

    public boolean isNull() {
        return cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK;
    }

    private CellUtils(Cell cell) {
        this.cell = cell;
    }

}
