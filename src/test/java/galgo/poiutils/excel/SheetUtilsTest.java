/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgo.poiutils.excel;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;

import java.util.List;
import java.util.Map;

/**
 *
 * @author valdemar.arantes
 */
public class SheetUtilsTest {

    private static final Logger log = LoggerFactory.getLogger(SheetUtilsTest.class);
    private final static String XLS_TEST_NAME = "teste.xlsx";
    private final static String SHEET_TEST_NAME = "CellUtils_Test";
    private static Workbook wb;
    private static Sheet sheet;


    public SheetUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        log.info("Abrindo a planilha de teste {}", XLS_TEST_NAME);
        wb = WorkbookFactory.create(Thread.currentThread().getContextClassLoader().
                getResourceAsStream(XLS_TEST_NAME));
        log.debug("Planilha aberta com sucesso");
        sheet = wb.getSheet(SHEET_TEST_NAME);
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @BeforeMethod
    public void setUpMethod() throws Exception {
    }

    @AfterMethod
    public void tearDownMethod() throws Exception {
    }

    @Test
    public void test() {
        final List<Map<String, String>> map = SheetUtils.getCellsAsListOfMap(sheet, 0, 0, 4);
        Assertions.assertThat("12345678901234").isEqualTo(map.get(0).get("cnpj"));
        log.debug(map.toString());
    }
}
