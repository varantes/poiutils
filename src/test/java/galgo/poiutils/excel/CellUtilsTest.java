/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package galgo.poiutils.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.assertj.core.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.math.BigDecimal;

/**
 *
 * @author valdemar.arantes
 */
public class CellUtilsTest {

    private static final Logger log = LoggerFactory.getLogger(CellUtilsTest.class);
    private final static String XLS_TEST_NAME = "teste.xlsx";
    private final static String SHEET_TEST_NAME = "CellUtils_Test";
    private static Workbook wb;
    private static Sheet sheet;

    public CellUtilsTest() {
    }

    @Test
    public void bigDecimalTest() {
        Cell cell = sheet.getRow(0).getCell(0);
        BigDecimal bd = CellUtils.from(cell).getBigDecimal();
        Assertions.assertThat(bd).isEqualTo(BigDecimal.valueOf(1.3));

        cell = sheet.getRow(0).getCell(1);
        bd = CellUtils.from(cell).getBigDecimal();
        Assertions.assertThat(bd).isEqualTo(BigDecimal.valueOf(2.4));

        cell = sheet.getRow(0).getCell(2);
        bd = CellUtils.from(cell).getBigDecimal();
        Assertions.assertThat(bd).isEqualTo(BigDecimal.valueOf(3.5));

        cell = sheet.getRow(0).getCell(3);
        bd = CellUtils.from(cell).getBigDecimal();
        Assertions.assertThat(bd).isEqualTo(BigDecimal.valueOf(3.5));;

        cell = sheet.getRow(0).getCell(10);
        bd = CellUtils.from(cell).getBigDecimal();
        Assertions.assertThat(bd).isNull();

        cell = sheet.getRow(1).getCell(0);
        bd = CellUtils.from(cell).getBigDecimal();
        Assertions.assertThat(bd).isEqualTo(BigDecimal.valueOf(1.23456));

        cell = sheet.getRow(1).getCell(0);
        bd = CellUtils.from(cell).getBigDecimal();
        Assertions.assertThat(bd).isEqualTo(BigDecimal.valueOf(1.23456));

        cell = sheet.getRow(1).getCell(1);
        bd = CellUtils.from(cell).getBigDecimal();
        Assertions.assertThat(bd).isEqualTo(BigDecimal.valueOf(2.23456));

        cell = sheet.getRow(1).getCell(2);
        bd = CellUtils.from(cell).getBigDecimal();
        Assertions.assertThat(bd).isEqualTo(BigDecimal.valueOf(3.23456));

        cell = sheet.getRow(1).getCell(3);
        bd = CellUtils.from(cell).getBigDecimal();
        Assertions.assertThat(bd).isEqualTo(BigDecimal.valueOf(1.23456));

    }

    @Test
    public void integerTest() {
        Cell cell = sheet.getRow(3).getCell(0);
        Integer bd = CellUtils.from(cell).getInt();
        Assertions.assertThat(bd).isEqualTo(10);

        cell = sheet.getRow(3).getCell(1);
        bd = CellUtils.from(cell).getInt();
        Assertions.assertThat(bd).isEqualTo(10);

        cell = sheet.getRow(3).getCell(2);
        try {
            bd = CellUtils.from(cell).getInt();
            Assertions.failBecauseExceptionWasNotThrown(NumberFormatException.class);
        } catch (NumberFormatException e) {
        }

        cell = sheet.getRow(3).getCell(3);
        try {
            bd = CellUtils.from(cell).getInt();
            Assertions.failBecauseExceptionWasNotThrown(NumberFormatException.class);
        } catch (NumberFormatException e) {
        }
    }
    
    @Test
    public void stringTest() {
        Assert.assertEquals(CellUtils.from(getCell(0, 0)).getString(), "1.3");
        Assert.assertEquals(CellUtils.from(getCell(3, 0)).getString(), "10");
        Assert.assertEquals(CellUtils.from(getCell(3, 1)).getString(), "10");
        
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        log.info("Abrindo a planilha de teste {}", XLS_TEST_NAME);
        wb = WorkbookFactory.create(Thread.currentThread().getContextClassLoader().
                getResourceAsStream(XLS_TEST_NAME));
        log.debug("Planilha aberta com sucesso");
        sheet = wb.getSheet(SHEET_TEST_NAME);
    }

    /**
     * Obtém a célula de uma linha e coluna dadas
     * @param rowNb Número da linha
     * @param colNb Número da coluna
     * @return Célula de uma linha e coluna dadas
     */
    private Cell getCell(int rowNb, int colNb) {
        return sheet.getRow(rowNb).getCell(colNb);
    }
}
